﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_2
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        //Переменные
        int CountRows = 0;
        int CountColumns = 0;

        /// <summary>
        /// Метод добавления строк-матрицы (решений)
        /// </summary>
        private void nUDRow_ValueChanged(object sender, EventArgs e)
        {
            CountRows = (int)nUDRow.Value;
            dGVMatrixOfEfficiency.RowCount = CountRows;
            for (int i = 0; i < CountRows; i++)
            {
                dGVMatrixOfEfficiency.Rows[i].HeaderCell.Value = "a" + Convert.ToString(i + 1);
            }
        }

        /// <summary>
        /// Метод добавления столбцов-матрицы (условий)
        /// </summary>
        private void nUDColumn_ValueChanged(object sender, EventArgs e)
        {
            CountColumns = (int)nUDColumn.Value;
            dGVMatrixOfEfficiency.ColumnCount = CountColumns;
            for (int i = 0; i < CountColumns; i++)
            {
                dGVMatrixOfEfficiency.Columns[i].HeaderCell.Value = "k" + Convert.ToString(i + 1);
                dGVMatrixOfEfficiency.Columns[i].Width = 55;
            }
            //дополнительная матрица(используется у баеса)
            dGVDopOblast.ColumnCount = dGVMatrixOfEfficiency.ColumnCount;
            dGVDopOblast.RowCount = 1;
            for (int i = 0; i < CountColumns; i++)
            {
                dGVDopOblast.Columns[i].HeaderCell.Value = "P" + Convert.ToString(i + 1);
                dGVDopOblast.Columns[i].Width = 55;
            }
            dGVDopOblastLeman.ColumnCount = dGVMatrixOfEfficiency.ColumnCount;
            dGVDopOblastLeman.RowCount = 1;
            for (int i = 0; i < CountColumns; i++)
            {
                dGVDopOblastLeman.Columns[i].HeaderCell.Value = "P" + Convert.ToString(i + 1);
                dGVDopOblastLeman.Columns[i].Width = 55;
            }
        }

        /// <summary>
        /// Метод авто-заполнения матрицы
        /// </summary>
        private void btnAutoFilling_Click(object sender, EventArgs e)
        {
            Random Random0to1 = new Random();
            for (var i = 0; i < dGVMatrixOfEfficiency.RowCount; i++)
                for (var j = 0; j < dGVMatrixOfEfficiency.ColumnCount; j++)
                {
                    if (radBtnBaise.Checked == true || radBtnLaplas.Checked == true || radBtnMaximax.Checked == true || radBtnValda.Checked == true || radBtnSavidj.Checked == true)
                    {
                        dGVMatrixOfEfficiency.Rows[i].Cells[j].Value = Convert.ToDouble(Random0to1.Next(1, 9)) / 10;
                        double sum = 0;
                        for (var z = 0; z < j; z++)
                            sum += Convert.ToDouble(dGVDopOblast.Rows[0].Cells[z].Value);
                        int Max = 10 - Convert.ToInt32(sum*10);
                        dGVDopOblast.Rows[0].Cells[j].Value = Convert.ToDouble(Random0to1.Next(0, Max)) / 10;
                        if (j == dGVDopOblast.ColumnCount - 1)
                        {
                            dGVDopOblast.Rows[0].Cells[j].Value = Convert.ToDouble(1-sum);
                        }
                    }
                    else if (radBtnGurvitz.Checked == true)
                    {
                        dGVMatrixOfEfficiency.Rows[i].Cells[j].Value = Convert.ToDouble(Random0to1.Next(1, 9)) / 10;
                        tBAlpha.Text = Convert.ToString(Convert.ToDouble(Random0to1.Next(1, 9)) / 10);
                    }
                    else if (radBtnHodjLeman.Checked == true)
                    {
                        dGVMatrixOfEfficiency.Rows[i].Cells[j].Value = Convert.ToDouble(Random0to1.Next(1, 9)) / 10;
                        tBVi.Text = Convert.ToString(Convert.ToDouble(Random0to1.Next(1, 9)) / 10);
                        double sum = 0;
                        for (var z = 0; z < j; z++)
                            sum += Convert.ToDouble(dGVDopOblastLeman.Rows[0].Cells[z].Value);
                        int Max = 10 - Convert.ToInt32(sum * 10);
                        dGVDopOblastLeman.Rows[0].Cells[j].Value = Convert.ToDouble(Random0to1.Next(0, Max)) / 10;
                        if (j == dGVDopOblastLeman.ColumnCount - 1)
                        {
                            dGVDopOblastLeman.Rows[0].Cells[j].Value = Convert.ToDouble(1 - sum);
                        }
                    }
                    else if (radBtnGermeiera.Checked == true)
                    {
                        dGVMatrixOfEfficiency.Rows[i].Cells[j].Value = Convert.ToDouble(0 - Random0to1.Next(1, 90)) * 10;
                        double sum = 0;
                        for (var z = 0; z < j; z++)
                            sum += Convert.ToDouble(dGVDopOblast.Rows[0].Cells[z].Value);
                        int Max = 10 - Convert.ToInt32(sum * 10);
                        dGVDopOblast.Rows[0].Cells[j].Value = Convert.ToDouble(Random0to1.Next(0, Max)) / 10;
                        if (j == dGVDopOblast.ColumnCount - 1)
                        {
                            dGVDopOblast.Rows[0].Cells[j].Value = Convert.ToDouble(1 - sum);
                        }

                    }
                    else if (radBtnProizvedenei.Checked == true)
                    {
                        dGVMatrixOfEfficiency.Rows[i].Cells[j].Value = Convert.ToDouble(Random0to1.Next(1, 9)) / 10;

                    }

                }
        }

        void CleanDopOblast()
        {
            dGVDopOblast.Visible = false;
            gBLeman.Visible = false;
            for (var i = 0; i < dGVDopOblastLeman.ColumnCount; i++)
                dGVDopOblastLeman.Rows[0].Cells[i].Value = null;
            grBoxAlpha.Visible = false;
            tBAlpha.Clear();
            for (var i = 0; i < dGVDopOblast.ColumnCount; i++)
                dGVDopOblast.Rows[0].Cells[i].Value = null;
        }

        /// <summary>
        /// Выбор критерия Байеса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radBtnBaise_CheckedChanged(object sender, EventArgs e)
        {
            CleanDopOblast();
            dGVDopOblast.Visible = true;

        }

        /// <summary>
        /// Выбор критерия Лапласа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radBtnLaplas_CheckedChanged(object sender, EventArgs e)
        {
            CleanDopOblast();
        }

        /// <summary>
        /// Выбор критерия Максимакса
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radBtnMaximax_CheckedChanged(object sender, EventArgs e)
        {
            CleanDopOblast();
        }

        /// <summary>
        /// Выбор критерия Вальда
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radBtnValda_CheckedChanged(object sender, EventArgs e)
        {
            CleanDopOblast();
        }

        /// <summary>
        /// Выбор Критерия Сэвиджа
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radBtnSavidj_CheckedChanged(object sender, EventArgs e)
        {
            CleanDopOblast();
        }

        /// <summary>
        /// Выбор Критерия Гурвица
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radBtnGurvitz_CheckedChanged(object sender, EventArgs e)
        {
            CleanDopOblast();
            grBoxAlpha.Visible = true;

        }

        /// <summary>
        /// Выбор Критерия Ходжа-Лемана
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radBtnHodjLeman_CheckedChanged(object sender, EventArgs e)
        {
            CleanDopOblast();
            gBLeman.Visible = true;
        }

        /// <summary>
        /// Выбор Критерия Гермейера
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radBtnGermeiera_CheckedChanged(object sender, EventArgs e)
        {
            CleanDopOblast();
            dGVDopOblast.Visible = true;
        }

        private void BL_CheckedChanged(object sender, EventArgs e)
        {
            CleanDopOblast();
            dGVDopOblast.Visible = true;
        }

        /// <summary>
        /// Выбор Критерий Произведений 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void radBtnProizvedenei_CheckedChanged(object sender, EventArgs e)
        {
            CleanDopOblast();
        }

        private void btnSuperPower_Click(object sender, EventArgs e)
        {
            //Расчет критерия Байеса
            if (radBtnBaise.Checked == true)
            {
                //Куча различного вывода
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Выбран критерий Байеса";
                rTBResoults.Text += Environment.NewLine;
                double[,] BeginMatrix = OutputUslovie();
                List<double> MassiveKa = new List<double>();
                rTBResoults.Text += "Даны вероятности:";
                rTBResoults.Text += Environment.NewLine;
                double[] masdblKoefP = new double[dGVDopOblast.ColumnCount];
                var strMasP = "";
                for (var i = 0; i < dGVDopOblast.ColumnCount; i++)
                {
                    masdblKoefP[i] = Convert.ToDouble(dGVDopOblast.Rows[0].Cells[i].Value);
                    strMasP += masdblKoefP[i].ToString() + " ";
                }
                rTBResoults.Text += strMasP;
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Их общая сумма равна: " + masdblKoefP.Sum();
                rTBResoults.Text += Environment.NewLine;
                if (masdblKoefP.Sum() == 1)
                {
                    rTBResoults.Text += Environment.NewLine;
                    rTBResoults.Text += "Условия удовлетворяют требованиям критерия";
                    rTBResoults.Text += Environment.NewLine;
                    //Находим среднее ожидание
                    for (var i = 0; i < dGVMatrixOfEfficiency.RowCount; i++)
                    {
                        double Ki = 0;
                        for (var j = 0; j < dGVMatrixOfEfficiency.ColumnCount; j++)
                        {
                            Ki += BeginMatrix[i, j] * masdblKoefP[j];
                        }
                        MassiveKa.Add(Ki);
                    }
                    rTBResoults.Text += Environment.NewLine;
                    rTBResoults.Text += "Подсчитанные Кi:";
                    rTBResoults.Text += Environment.NewLine;
                    for (var i = 0; i < MassiveKa.Count; i++)
                        rTBResoults.Text += MassiveKa[i] + " ";
                    rTBResoults.Text += Environment.NewLine;
                    rTBResoults.Text += "Наилучшим решением будет(ут) вариант(ы):";
                    rTBResoults.Text += Environment.NewLine;
                    List<int> otvet = SearchMax(MassiveKa);
                    for (var i = 0; i < otvet.Count; i++)
                        rTBResoults.Text += otvet[i] + " ";
                }
            }
            //Расчет критерия Лапласа 
            else if (radBtnLaplas.Checked == true)
            {
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Выбран критерий Лапласа";
                rTBResoults.Text += Environment.NewLine;
                double[,] BeginMatrix = OutputUslovie();
                rTBResoults.Text += Environment.NewLine;
                var Pi = 1.0 / dGVMatrixOfEfficiency.ColumnCount;
                rTBResoults.Text += "Вероятность Pi = " + Pi.ToString();
                rTBResoults.Text += Environment.NewLine;
                List<double> MassiveKa = new List<double>();
                //Находим среднее ожидание
                for (var i = 0; i < dGVMatrixOfEfficiency.RowCount; i++)
                {
                    double Ki = 0;
                    for (var j = 0; j < dGVMatrixOfEfficiency.ColumnCount; j++)
                    {
                        Ki += BeginMatrix[i, j] * Pi;
                    }
                    MassiveKa.Add(Ki);
                }
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Подсчитанные Кi:";
                rTBResoults.Text += Environment.NewLine;
                for (var i = 0; i < MassiveKa.Count; i++)
                    rTBResoults.Text += MassiveKa[i] + " ";
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Наилучшим решением будет(ут) вариант(ы):";
                rTBResoults.Text += Environment.NewLine;
                List<int> otvet = SearchMax(MassiveKa);
                for (var i = 0; i < otvet.Count; i++)
                    rTBResoults.Text += otvet[i] + " ";
            }
            //Расчет критерия Максимакса
            else if (radBtnMaximax.Checked == true)
            {
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Выбран критерий Маскимакса";
                rTBResoults.Text += Environment.NewLine;
                double[,] BeginMatrix = OutputUslovie();
                rTBResoults.Text += Environment.NewLine;
                List<double> OptimalEffection = SearchMaxInDoubleMassive(BeginMatrix);
                //Вывод промежуточного значения
                rTBResoults.Text += "В финал вышли следующие значения от каждой строки(решения)";
                rTBResoults.Text += Environment.NewLine;
                for (var i = 0; i < OptimalEffection.Count; i++)
                    rTBResoults.Text += OptimalEffection[i] + "; ";
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Согласно критерию Максимакса лучше выбрать: ";
                rTBResoults.Text += Environment.NewLine;
                List<int> otvet = SearchMax(OptimalEffection);
                for (var i = 0; i < otvet.Count; i++)
                    rTBResoults.Text += otvet[i] + " ";
            }
            //Расчет критерия Вальда
            else if (radBtnValda.Checked == true)
            {
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Выбран критерий Вальда";
                rTBResoults.Text += Environment.NewLine;
                double[,] BeginMatrix = OutputUslovie();
                List<double> OptimalEffection = SearchMinInDoubleMassive(BeginMatrix);
                rTBResoults.Text += "В финал вышли следующие значения от каждой строки(решения)";
                rTBResoults.Text += Environment.NewLine;
                for (var i = 0; i < OptimalEffection.Count; i++)
                    rTBResoults.Text += OptimalEffection[i] + "; ";
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Согласно критерию Вальда лучше выбрать: ";
                rTBResoults.Text += Environment.NewLine;
                List<int> otvet = SearchMax(OptimalEffection);
                for (var i = 0; i < otvet.Count; i++)
                    rTBResoults.Text += otvet[i] + " ";
            }
            //Расчет критерия Сэвиджа
            else if (radBtnSavidj.Checked == true)
            {
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Выбран критерий Сэвиджа";
                rTBResoults.Text += Environment.NewLine;
                double[,] BeginMatrix = OutputUslovie();
                double[,] RisksMatrix = ModificationMatrixForSavidj(BeginMatrix);
                List<double> OptimalEffection = SearchMaxInDoubleMassive(RisksMatrix);
                rTBResoults.Text += "В финал вышли следующие значения от каждой строки(решения)";
                rTBResoults.Text += Environment.NewLine;
                for (var i = 0; i < OptimalEffection.Count; i++)
                    rTBResoults.Text += OptimalEffection[i] + "; ";
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Согласно критерию Сэвиджа лучше выбрать: ";
                rTBResoults.Text += Environment.NewLine;
                List<int> otvet = SearchMin(OptimalEffection);
                for (var i = 0; i < otvet.Count; i++)
                    rTBResoults.Text += otvet[i] + " ";
            }
            //Расчет критерия Гурвица
            else if (radBtnGurvitz.Checked == true)
            {
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Выбран критерий Гурвица";
                rTBResoults.Text += Environment.NewLine;
                double[,] BeginMatrix = OutputUslovie();
                double alpha = Convert.ToDouble(tBAlpha.Text);
                rTBResoults.Text += "Значение α = " + alpha;
                List<double> OptimalEffection = SearchGervitzK(BeginMatrix, alpha);
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "В финал вышли следующие значения от каждой строки(решения)";
                rTBResoults.Text += Environment.NewLine;
                for (var i = 0; i < OptimalEffection.Count; i++)
                    rTBResoults.Text += OptimalEffection[i] + "; ";
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Согласно критерию Гурвица лучше выбрать: ";
                rTBResoults.Text += Environment.NewLine;
                List<int> otvet = SearchMax(OptimalEffection);
                for (var i = 0; i < otvet.Count; i++)
                    rTBResoults.Text += otvet[i] + " ";
            }
            //Расчет Критерия Ходжа-Лемана
            else if (radBtnHodjLeman.Checked == true)
            {
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Выбран критерий Ходжа-Лемана";
                rTBResoults.Text += Environment.NewLine;
                double[,] BeginMatrix = OutputUslovie();
                rTBResoults.Text += "Даны вероятности:";
                rTBResoults.Text += Environment.NewLine;
                double[] masdblKoefP = new double[dGVDopOblastLeman.ColumnCount];
                var strMasP = " ";
                for (var i = 0; i < dGVDopOblastLeman.ColumnCount; i++)
                {
                    masdblKoefP[i] = Convert.ToDouble(dGVDopOblastLeman.Rows[0].Cells[i].Value);
                    strMasP += Convert.ToString(masdblKoefP[i]) + " ";
                }
                rTBResoults.Text += strMasP;
                rTBResoults.Text += Environment.NewLine;
                double dbVi = Convert.ToDouble(tBVi.Text);
                rTBResoults.Text += "Значение v = " + dbVi;
                List<double> OptimalEffection = SearchHodjLeman(BeginMatrix, masdblKoefP, dbVi);
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "В финал вышли следующие значения от каждой строки(решения)";
                rTBResoults.Text += Environment.NewLine;
                for (var i = 0; i < OptimalEffection.Count; i++)
                    rTBResoults.Text += OptimalEffection[i] + "; ";
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Согласно критерию Ходжа-Лемана лучше выбрать: ";
                rTBResoults.Text += Environment.NewLine;
                List<int> otvet = SearchMax(OptimalEffection);
                for (var i = 0; i < otvet.Count; i++)
                    rTBResoults.Text += otvet[i] + " ";
            }
            // Расчет Критерия Гермейреа
            else if (radBtnGermeiera.Checked == true)
            {
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Выбран критерий Гермейера";
                rTBResoults.Text += Environment.NewLine;
                double[,] BeginMatrix = OutputUslovie();
                rTBResoults.Text += "Даны вероятности:";
                rTBResoults.Text += Environment.NewLine;
                double[] masdblKoefP = new double[dGVDopOblast.ColumnCount];
                var strMasP = "";
                for (var i = 0; i < dGVDopOblast.ColumnCount; i++)
                {
                    masdblKoefP[i] = Convert.ToDouble(dGVDopOblast.Rows[0].Cells[i].Value);
                    strMasP += masdblKoefP[i].ToString() + " ";
                }
                rTBResoults.Text += Environment.NewLine;
                List<double> OptimalEffection = SearchGermeiera(BeginMatrix, masdblKoefP);
                rTBResoults.Text += "В финал вышли следующие значения от каждой строки(решения)";
                rTBResoults.Text += Environment.NewLine;
                for (var i = 0; i < OptimalEffection.Count; i++)
                    rTBResoults.Text += OptimalEffection[i] + "; ";
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Согласно критерию Ходжа-Лемана лучше выбрать: ";
                rTBResoults.Text += Environment.NewLine;
                List<int> otvet = SearchMax(OptimalEffection);
                for (var i = 0; i < otvet.Count; i++)
                    rTBResoults.Text += otvet[i] + " ";
            }
            // Расчет Критерия Произведений
            else if (radBtnProizvedenei.Checked == true)
            {
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Выбран критерий Произведений";
                rTBResoults.Text += Environment.NewLine;
                double[,] BeginMatrix = OutputUslovie();
                rTBResoults.Text += Environment.NewLine;
                List<double> OptimalEffection = SearchProizvedeni(BeginMatrix);
                rTBResoults.Text += "В финал вышли следующие значения от каждой строки(решения)";
                rTBResoults.Text += Environment.NewLine;
                for (var i = 0; i < OptimalEffection.Count; i++)
                    rTBResoults.Text += OptimalEffection[i] + "; ";
                rTBResoults.Text += Environment.NewLine;
                rTBResoults.Text += "Согласно критерию Произведений лучше выбрать: ";
                rTBResoults.Text += Environment.NewLine;
                List<int> otvet = SearchMax(OptimalEffection);
                for (var i = 0; i < otvet.Count; i++)
                    rTBResoults.Text += otvet[i] + " ";
            }
                  }


        /// <summary>
        /// Метод вывода матрицы эффективности (выигрышей) и возвращение ее в виде массива
        /// </summary>
        /// <returns> массив с значениями матрицы эффективности </returns>
        double[,] OutputUslovie()
        {
            double[,] masDoubleBeginMatrix = new double[dGVMatrixOfEfficiency.RowCount, dGVMatrixOfEfficiency.ColumnCount];
            for (var i = 0; i < dGVMatrixOfEfficiency.RowCount; i++)
                for (var j = 0; j < dGVMatrixOfEfficiency.ColumnCount; j++)
                {
                    masDoubleBeginMatrix[i, j] = Convert.ToDouble(dGVMatrixOfEfficiency.Rows[i].Cells[j].Value);
                }
            rTBResoults.Text += "Матрица эффективности (выигрышей)";
            var strOutput = "";
            strOutput += Environment.NewLine;
            for (int i = 0; i < masDoubleBeginMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < masDoubleBeginMatrix.GetLength(1); j++)
                {
                    strOutput += $" {masDoubleBeginMatrix[i, j]} ";
                }
                strOutput += Environment.NewLine;
            }
            strOutput += Environment.NewLine;
            rTBResoults.Text += strOutput;
            return masDoubleBeginMatrix;
        }


        /// <summary>
        /// Метод превращения матрицы выигрышей в матрицы рисков и ее вывод
        /// </summary>
        /// <param name="Uslovie"></param>
        /// <returns></returns>
        double[,] ModificationMatrixForSavidj(double[,] Uslovie)
        {
            double[,] ItogMatrix = new double[Uslovie.GetLength(0), Uslovie.GetLength(1)];
            //Расчет матрицы рисков 
            double MaxInColumn;
            for (var j = 0; j < Uslovie.GetLength(1); j++)
            {
                MaxInColumn = Uslovie[0, j];
                for (var i = 0; i < Uslovie.GetLength(0); i++)
                    if (Uslovie[i, j] > MaxInColumn)
                        MaxInColumn = Uslovie[i, j];
                for (var i = 0; i < Uslovie.GetLength(0); i++)
                    ItogMatrix[i, j] = MaxInColumn - Uslovie[i, j];
            }
            //Вывод матрицы рисков
            rTBResoults.Text += "Матрица рисков";
            var strOutput = "";
            strOutput += Environment.NewLine;
            for (int i = 0; i < ItogMatrix.GetLength(0); i++)
            {
                for (int j = 0; j < ItogMatrix.GetLength(1); j++)
                {
                    strOutput += $" {ItogMatrix[i, j]} ";
                }
                strOutput += Environment.NewLine;
            }
            strOutput += Environment.NewLine;
            rTBResoults.Text += strOutput;
            return ItogMatrix;
        }

        /// <summary>
        /// Поиск максимального значения в списке (доп. столбец)
        /// </summary>
        /// <param name="Resoults"></param>
        /// <returns></returns>
        List<int> SearchMax(List<double> Resoults)
        {
            var max = Resoults.Max();
            List<int> otvet = new List<int>();
            var PresentMax = Resoults[0];
            otvet.Add(1);
            for (var i = 1; i < Resoults.Count; i++)
            {
                if (PresentMax < Resoults[i])
                {
                    PresentMax = Resoults[i];
                    otvet.Clear();
                    otvet.Add(i + 1);
                }
                else if (PresentMax == Resoults[i])
                {
                    otvet.Add(i + 1);
                }
            }
            return otvet;
        }

        /// <summary>
        /// Поиск минимального значения в списке (доп. столбец)
        /// </summary>
        /// <param name="Resoults"></param>
        /// <returns></returns>
        List<int> SearchMin(List<double> Resoults)
        {
            var min = Resoults.Min();
            List<int> otvet = new List<int>();
            var PresentMin = Resoults[0];
            otvet.Add(1);
            for (var i = 1; i < Resoults.Count; i++)
            {
                if (PresentMin > Resoults[i])
                {
                    PresentMin = Resoults[i];
                    otvet.Clear();
                    otvet.Add(i + 1);
                }
                else if (PresentMin == Resoults[i])
                {
                    otvet.Add(i + 1);
                }
            }
            return otvet;
        }


        /// <summary>
        /// Поиск Максимальных значений в каждой строке
        /// </summary>
        /// <param name="Uslovie"></param>
        /// <returns></returns>
        List<double> SearchMaxInDoubleMassive(double[,] Uslovie)
        {
            List<double> otvet = new List<double>();
            double Max = 0;
            for (var i = 0; i < Uslovie.GetLength(0); i++)
            {
                Max = Uslovie[i, 0];
                for (var j = 1; j < Uslovie.GetLength(1); j++)
                {
                    if (Uslovie[i, j] > Max)
                        Max = Uslovie[i, j];
                }
                otvet.Add(Max);
            }
            return otvet;
        }

        /// <summary>
        /// Поиск Минимальных значений в каждой строке
        /// </summary>
        /// <param name="Uslovie"></param>
        /// <returns></returns>
        List<double> SearchMinInDoubleMassive(double[,] Uslovie)
        {
            List<double> otvet = new List<double>();
            double Min = 0;
            for (var i = 0; i < Uslovie.GetLength(0); i++)
            {
                Min = Uslovie[i, 0];
                for (var j = 1; j < Uslovie.GetLength(1); j++)
                {
                    if (Uslovie[i, j] < Min)
                        Min = Uslovie[i, j];
                }
                otvet.Add(Min);
            }
            return otvet;
        }

        /// <summary>
        /// Поиск K-ек по Гурвицу
        /// </summary>
        /// <param name="Uslovie"></param>
        /// <param name="alpha"></param>
        /// <returns></returns>
        List<double> SearchGervitzK(double[,] Uslovie, double alpha)
        {
            List<double> otvet = new List<double>();
            double Min, Max = 0;
            for (var i = 0; i < Uslovie.GetLength(0); i++)
            {
                Max = Uslovie[i, 0];
                Min = Uslovie[i, 0];
                for (var j = 1; j < Uslovie.GetLength(1); j++)
                {
                    if (Uslovie[i, j] > Max)
                        Max = Uslovie[i, j];
                    if (Uslovie[i, j] < Min)
                        Min = Uslovie[i, j];
                }
                double K = Max * alpha + (1 - alpha) * Min;
                otvet.Add(K);
            }
            return otvet;
        }

        /// <summary>
        /// Поиск K-ек по Ходжа-Лемана
        /// </summary>
        /// <param name="Uslovie"></param>
        /// <param name="P"></param>
        /// <param name="dbVi"></param>
        /// <returns></returns>
        List<double> SearchHodjLeman(double[,] Uslovie, double[] P, double dbVi)
        {
            List<double> otvet = new List<double>();
            double Sum = 0;
            double Min = 1;

            for (var i = 0; i < Uslovie.GetLength(0); i++)
            {
                for (var j = 0; j < Uslovie.GetLength(1); j++)
                {
                    Sum += P[j] * Uslovie[i, j];
                    if (Uslovie[i, j] < Min)
                        Min = Uslovie[i, j];
                }
                double K = dbVi * Sum + (1 - dbVi) * Min;
                otvet.Add(K);
            }
            return otvet;
        }

        /// <summary>
        /// Поиск K-ек по Гермейеру
        /// </summary>
        /// <param name="Uslovie"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        List<double> SearchGermeiera(double[,] Uslovie, double[] P)
        {
            List<double> otvet = new List<double>();
            double[,] MasKP = new double[Uslovie.GetLength(0), Uslovie.GetLength(1)];
            double Min = 0;
            for (var i = 0; i < Uslovie.GetLength(0); i++)
            {
                for (var j = 0; j < Uslovie.GetLength(1); j++)
                {
                    MasKP[i, j] = Uslovie[i, j] * P[j];
                    if (Min > MasKP[i, j])
                        Min = MasKP[i, j];
                }
                otvet.Add(Min);
            }
            return otvet;
        }

        /// <summary>
        /// Поиск K-ек по Критерию Произведений
        /// </summary>
        /// <param name="Uslovie"></param>
        /// <param name="P"></param>
        /// <returns></returns>
        List<double> SearchProizvedeni(double[,] Uslovie)
        {
            List<double> otvet = new List<double>();
            double K = 1;
            for (var i = 0; i < Uslovie.GetLength(0); i++)
            {
                for (var j = 0; j < Uslovie.GetLength(1); j++)
                {
                    K *= Uslovie[i, j];
                }
                otvet.Add(K);
            }
            return otvet;
        }
    }
}
