﻿namespace Lab_2
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.dGVMatrixOfEfficiency = new System.Windows.Forms.DataGridView();
            this.nUDRow = new System.Windows.Forms.NumericUpDown();
            this.nUDColumn = new System.Windows.Forms.NumericUpDown();
            this.lbCountRows = new System.Windows.Forms.Label();
            this.lblCountColumns = new System.Windows.Forms.Label();
            this.rTBResoults = new System.Windows.Forms.RichTextBox();
            this.lblResult = new System.Windows.Forms.Label();
            this.grBChangeСlassicMetod = new System.Windows.Forms.GroupBox();
            this.radBtnProizvedenei = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.radBtnSavidj = new System.Windows.Forms.RadioButton();
            this.radBtnGermeiera = new System.Windows.Forms.RadioButton();
            this.radBtnValda = new System.Windows.Forms.RadioButton();
            this.radBtnHodjLeman = new System.Windows.Forms.RadioButton();
            this.radBtnMaximax = new System.Windows.Forms.RadioButton();
            this.radBtnGurvitz = new System.Windows.Forms.RadioButton();
            this.radBtnLaplas = new System.Windows.Forms.RadioButton();
            this.radBtnBaise = new System.Windows.Forms.RadioButton();
            this.grBoxDopUslovia = new System.Windows.Forms.GroupBox();
            this.gBLeman = new System.Windows.Forms.GroupBox();
            this.dGVDopOblastLeman = new System.Windows.Forms.DataGridView();
            this.tBVi = new System.Windows.Forms.TextBox();
            this.labelV = new System.Windows.Forms.Label();
            this.dGVDopOblast = new System.Windows.Forms.DataGridView();
            this.grBoxAlpha = new System.Windows.Forms.GroupBox();
            this.tBAlpha = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAutoFilling = new System.Windows.Forms.Button();
            this.btnSuperPower = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dGVMatrixOfEfficiency)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDRow)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDColumn)).BeginInit();
            this.grBChangeСlassicMetod.SuspendLayout();
            this.grBoxDopUslovia.SuspendLayout();
            this.gBLeman.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGVDopOblastLeman)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGVDopOblast)).BeginInit();
            this.grBoxAlpha.SuspendLayout();
            this.SuspendLayout();
            // 
            // dGVMatrixOfEfficiency
            // 
            this.dGVMatrixOfEfficiency.AllowUserToAddRows = false;
            this.dGVMatrixOfEfficiency.AllowUserToDeleteRows = false;
            this.dGVMatrixOfEfficiency.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dGVMatrixOfEfficiency.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVMatrixOfEfficiency.Location = new System.Drawing.Point(0, 94);
            this.dGVMatrixOfEfficiency.Name = "dGVMatrixOfEfficiency";
            this.dGVMatrixOfEfficiency.RowHeadersWidth = 48;
            this.dGVMatrixOfEfficiency.Size = new System.Drawing.Size(579, 236);
            this.dGVMatrixOfEfficiency.TabIndex = 0;
            // 
            // nUDRow
            // 
            this.nUDRow.Location = new System.Drawing.Point(12, 29);
            this.nUDRow.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDRow.Name = "nUDRow";
            this.nUDRow.Size = new System.Drawing.Size(56, 20);
            this.nUDRow.TabIndex = 31;
            this.nUDRow.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDRow.ValueChanged += new System.EventHandler(this.nUDRow_ValueChanged);
            // 
            // nUDColumn
            // 
            this.nUDColumn.Location = new System.Drawing.Point(12, 64);
            this.nUDColumn.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDColumn.Name = "nUDColumn";
            this.nUDColumn.Size = new System.Drawing.Size(56, 20);
            this.nUDColumn.TabIndex = 32;
            this.nUDColumn.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDColumn.ValueChanged += new System.EventHandler(this.nUDColumn_ValueChanged);
            // 
            // lbCountRows
            // 
            this.lbCountRows.AutoSize = true;
            this.lbCountRows.Location = new System.Drawing.Point(74, 31);
            this.lbCountRows.Name = "lbCountRows";
            this.lbCountRows.Size = new System.Drawing.Size(132, 13);
            this.lbCountRows.TabIndex = 33;
            this.lbCountRows.Text = "Введите кол-во решений";
            // 
            // lblCountColumns
            // 
            this.lblCountColumns.AutoSize = true;
            this.lblCountColumns.Location = new System.Drawing.Point(74, 66);
            this.lblCountColumns.Name = "lblCountColumns";
            this.lblCountColumns.Size = new System.Drawing.Size(129, 13);
            this.lblCountColumns.TabIndex = 34;
            this.lblCountColumns.Text = "Введите кол-во условий";
            // 
            // rTBResoults
            // 
            this.rTBResoults.BackColor = System.Drawing.SystemColors.MenuText;
            this.rTBResoults.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.rTBResoults.Location = new System.Drawing.Point(12, 452);
            this.rTBResoults.Name = "rTBResoults";
            this.rTBResoults.Size = new System.Drawing.Size(910, 184);
            this.rTBResoults.TabIndex = 35;
            this.rTBResoults.Text = "";
            // 
            // lblResult
            // 
            this.lblResult.AutoSize = true;
            this.lblResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.lblResult.Location = new System.Drawing.Point(12, 436);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(228, 16);
            this.lblResult.TabIndex = 36;
            this.lblResult.Text = "Консоль вывода результатов";
            // 
            // grBChangeСlassicMetod
            // 
            this.grBChangeСlassicMetod.Controls.Add(this.radBtnProizvedenei);
            this.grBChangeСlassicMetod.Controls.Add(this.label1);
            this.grBChangeСlassicMetod.Controls.Add(this.radBtnSavidj);
            this.grBChangeСlassicMetod.Controls.Add(this.radBtnGermeiera);
            this.grBChangeСlassicMetod.Controls.Add(this.radBtnValda);
            this.grBChangeСlassicMetod.Controls.Add(this.radBtnHodjLeman);
            this.grBChangeСlassicMetod.Controls.Add(this.radBtnMaximax);
            this.grBChangeСlassicMetod.Controls.Add(this.radBtnGurvitz);
            this.grBChangeСlassicMetod.Controls.Add(this.radBtnLaplas);
            this.grBChangeСlassicMetod.Controls.Add(this.radBtnBaise);
            this.grBChangeСlassicMetod.Location = new System.Drawing.Point(621, 31);
            this.grBChangeСlassicMetod.Name = "grBChangeСlassicMetod";
            this.grBChangeСlassicMetod.Size = new System.Drawing.Size(301, 275);
            this.grBChangeСlassicMetod.TabIndex = 37;
            this.grBChangeСlassicMetod.TabStop = false;
            this.grBChangeСlassicMetod.Text = "Классические Критерии Принятия Решения";
            // 
            // radBtnProizvedenei
            // 
            this.radBtnProizvedenei.AutoSize = true;
            this.radBtnProizvedenei.Location = new System.Drawing.Point(5, 227);
            this.radBtnProizvedenei.Name = "radBtnProizvedenei";
            this.radBtnProizvedenei.Size = new System.Drawing.Size(150, 17);
            this.radBtnProizvedenei.TabIndex = 43;
            this.radBtnProizvedenei.TabStop = true;
            this.radBtnProizvedenei.Text = "Критерий Произведений";
            this.radBtnProizvedenei.UseVisualStyleBackColor = true;
            this.radBtnProizvedenei.CheckedChanged += new System.EventHandler(this.radBtnProizvedenei_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 142);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(228, 13);
            this.label1.TabIndex = 44;
            this.label1.Text = "Производные Критерии Принятия Решения";
            // 
            // radBtnSavidj
            // 
            this.radBtnSavidj.AutoSize = true;
            this.radBtnSavidj.Location = new System.Drawing.Point(5, 111);
            this.radBtnSavidj.Name = "radBtnSavidj";
            this.radBtnSavidj.Size = new System.Drawing.Size(121, 17);
            this.radBtnSavidj.TabIndex = 43;
            this.radBtnSavidj.TabStop = true;
            this.radBtnSavidj.Text = "Критерий Сэвиджа";
            this.radBtnSavidj.UseVisualStyleBackColor = true;
            this.radBtnSavidj.CheckedChanged += new System.EventHandler(this.radBtnSavidj_CheckedChanged);
            // 
            // radBtnGermeiera
            // 
            this.radBtnGermeiera.AutoSize = true;
            this.radBtnGermeiera.Location = new System.Drawing.Point(5, 204);
            this.radBtnGermeiera.Name = "radBtnGermeiera";
            this.radBtnGermeiera.Size = new System.Drawing.Size(132, 17);
            this.radBtnGermeiera.TabIndex = 41;
            this.radBtnGermeiera.TabStop = true;
            this.radBtnGermeiera.Text = "Критерий Гермейера";
            this.radBtnGermeiera.UseVisualStyleBackColor = true;
            this.radBtnGermeiera.CheckedChanged += new System.EventHandler(this.radBtnGermeiera_CheckedChanged);
            // 
            // radBtnValda
            // 
            this.radBtnValda.AutoSize = true;
            this.radBtnValda.Location = new System.Drawing.Point(5, 88);
            this.radBtnValda.Name = "radBtnValda";
            this.radBtnValda.Size = new System.Drawing.Size(113, 17);
            this.radBtnValda.TabIndex = 42;
            this.radBtnValda.TabStop = true;
            this.radBtnValda.Text = "Критерий Вальда";
            this.radBtnValda.UseVisualStyleBackColor = true;
            this.radBtnValda.CheckedChanged += new System.EventHandler(this.radBtnValda_CheckedChanged);
            // 
            // radBtnHodjLeman
            // 
            this.radBtnHodjLeman.AutoSize = true;
            this.radBtnHodjLeman.Location = new System.Drawing.Point(5, 181);
            this.radBtnHodjLeman.Name = "radBtnHodjLeman";
            this.radBtnHodjLeman.Size = new System.Drawing.Size(152, 17);
            this.radBtnHodjLeman.TabIndex = 40;
            this.radBtnHodjLeman.TabStop = true;
            this.radBtnHodjLeman.Text = "Критерий Ходжа-Лемана";
            this.radBtnHodjLeman.UseVisualStyleBackColor = true;
            this.radBtnHodjLeman.CheckedChanged += new System.EventHandler(this.radBtnHodjLeman_CheckedChanged);
            // 
            // radBtnMaximax
            // 
            this.radBtnMaximax.AutoSize = true;
            this.radBtnMaximax.Location = new System.Drawing.Point(5, 65);
            this.radBtnMaximax.Name = "radBtnMaximax";
            this.radBtnMaximax.Size = new System.Drawing.Size(141, 17);
            this.radBtnMaximax.TabIndex = 41;
            this.radBtnMaximax.TabStop = true;
            this.radBtnMaximax.Text = "Критерий Максимакса";
            this.radBtnMaximax.UseVisualStyleBackColor = true;
            this.radBtnMaximax.CheckedChanged += new System.EventHandler(this.radBtnMaximax_CheckedChanged);
            // 
            // radBtnGurvitz
            // 
            this.radBtnGurvitz.AutoSize = true;
            this.radBtnGurvitz.Location = new System.Drawing.Point(5, 158);
            this.radBtnGurvitz.Name = "radBtnGurvitz";
            this.radBtnGurvitz.Size = new System.Drawing.Size(117, 17);
            this.radBtnGurvitz.TabIndex = 39;
            this.radBtnGurvitz.TabStop = true;
            this.radBtnGurvitz.Text = "Критерий Гурвица";
            this.radBtnGurvitz.UseVisualStyleBackColor = true;
            this.radBtnGurvitz.CheckedChanged += new System.EventHandler(this.radBtnGurvitz_CheckedChanged);
            // 
            // radBtnLaplas
            // 
            this.radBtnLaplas.AutoSize = true;
            this.radBtnLaplas.Location = new System.Drawing.Point(6, 42);
            this.radBtnLaplas.Name = "radBtnLaplas";
            this.radBtnLaplas.Size = new System.Drawing.Size(120, 17);
            this.radBtnLaplas.TabIndex = 40;
            this.radBtnLaplas.TabStop = true;
            this.radBtnLaplas.Text = "Критерий Лапласа";
            this.radBtnLaplas.UseVisualStyleBackColor = true;
            this.radBtnLaplas.CheckedChanged += new System.EventHandler(this.radBtnLaplas_CheckedChanged);
            // 
            // radBtnBaise
            // 
            this.radBtnBaise.AutoSize = true;
            this.radBtnBaise.Location = new System.Drawing.Point(6, 19);
            this.radBtnBaise.Name = "radBtnBaise";
            this.radBtnBaise.Size = new System.Drawing.Size(113, 17);
            this.radBtnBaise.TabIndex = 39;
            this.radBtnBaise.TabStop = true;
            this.radBtnBaise.Text = "Критерий Байеса";
            this.radBtnBaise.UseVisualStyleBackColor = true;
            this.radBtnBaise.CheckedChanged += new System.EventHandler(this.radBtnBaise_CheckedChanged);
            // 
            // grBoxDopUslovia
            // 
            this.grBoxDopUslovia.BackColor = System.Drawing.SystemColors.ActiveBorder;
            this.grBoxDopUslovia.Controls.Add(this.gBLeman);
            this.grBoxDopUslovia.Controls.Add(this.dGVDopOblast);
            this.grBoxDopUslovia.Controls.Add(this.grBoxAlpha);
            this.grBoxDopUslovia.Location = new System.Drawing.Point(0, 336);
            this.grBoxDopUslovia.Name = "grBoxDopUslovia";
            this.grBoxDopUslovia.Size = new System.Drawing.Size(579, 97);
            this.grBoxDopUslovia.TabIndex = 39;
            this.grBoxDopUslovia.TabStop = false;
            this.grBoxDopUslovia.Text = "Дополнительные условия (если нужны)";
            // 
            // gBLeman
            // 
            this.gBLeman.Controls.Add(this.dGVDopOblastLeman);
            this.gBLeman.Controls.Add(this.tBVi);
            this.gBLeman.Controls.Add(this.labelV);
            this.gBLeman.Location = new System.Drawing.Point(6, 19);
            this.gBLeman.Name = "gBLeman";
            this.gBLeman.Size = new System.Drawing.Size(567, 72);
            this.gBLeman.TabIndex = 43;
            this.gBLeman.TabStop = false;
            this.gBLeman.Visible = false;
            // 
            // dGVDopOblastLeman
            // 
            this.dGVDopOblastLeman.AllowUserToAddRows = false;
            this.dGVDopOblastLeman.AllowUserToDeleteRows = false;
            this.dGVDopOblastLeman.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dGVDopOblastLeman.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVDopOblastLeman.Location = new System.Drawing.Point(6, 13);
            this.dGVDopOblastLeman.Name = "dGVDopOblastLeman";
            this.dGVDopOblastLeman.RowHeadersWidth = 48;
            this.dGVDopOblastLeman.Size = new System.Drawing.Size(391, 53);
            this.dGVDopOblastLeman.TabIndex = 44;
            // 
            // tBVi
            // 
            this.tBVi.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tBVi.Location = new System.Drawing.Point(461, 23);
            this.tBVi.Name = "tBVi";
            this.tBVi.Size = new System.Drawing.Size(100, 29);
            this.tBVi.TabIndex = 43;
            // 
            // labelV
            // 
            this.labelV.AutoSize = true;
            this.labelV.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelV.Location = new System.Drawing.Point(422, 21);
            this.labelV.Name = "labelV";
            this.labelV.Size = new System.Drawing.Size(35, 31);
            this.labelV.TabIndex = 0;
            this.labelV.Text = "ν ";
            // 
            // dGVDopOblast
            // 
            this.dGVDopOblast.AllowUserToAddRows = false;
            this.dGVDopOblast.AllowUserToDeleteRows = false;
            this.dGVDopOblast.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dGVDopOblast.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dGVDopOblast.Location = new System.Drawing.Point(6, 19);
            this.dGVDopOblast.Name = "dGVDopOblast";
            this.dGVDopOblast.RowHeadersWidth = 48;
            this.dGVDopOblast.Size = new System.Drawing.Size(567, 72);
            this.dGVDopOblast.TabIndex = 42;
            this.dGVDopOblast.Visible = false;
            // 
            // grBoxAlpha
            // 
            this.grBoxAlpha.Controls.Add(this.tBAlpha);
            this.grBoxAlpha.Controls.Add(this.label2);
            this.grBoxAlpha.Location = new System.Drawing.Point(6, 19);
            this.grBoxAlpha.Name = "grBoxAlpha";
            this.grBoxAlpha.Size = new System.Drawing.Size(567, 72);
            this.grBoxAlpha.TabIndex = 42;
            this.grBoxAlpha.TabStop = false;
            this.grBoxAlpha.Visible = false;
            // 
            // tBAlpha
            // 
            this.tBAlpha.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.tBAlpha.Location = new System.Drawing.Point(115, 30);
            this.tBAlpha.Name = "tBAlpha";
            this.tBAlpha.Size = new System.Drawing.Size(100, 29);
            this.tBAlpha.TabIndex = 43;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(72, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(37, 31);
            this.label2.TabIndex = 0;
            this.label2.Text = "α ";
            // 
            // btnAutoFilling
            // 
            this.btnAutoFilling.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btnAutoFilling.Location = new System.Drawing.Point(621, 312);
            this.btnAutoFilling.Name = "btnAutoFilling";
            this.btnAutoFilling.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnAutoFilling.Size = new System.Drawing.Size(296, 47);
            this.btnAutoFilling.TabIndex = 40;
            this.btnAutoFilling.Text = "Авто-заполнение";
            this.btnAutoFilling.UseVisualStyleBackColor = false;
            this.btnAutoFilling.Click += new System.EventHandler(this.btnAutoFilling_Click);
            // 
            // btnSuperPower
            // 
            this.btnSuperPower.BackColor = System.Drawing.SystemColors.ScrollBar;
            this.btnSuperPower.Location = new System.Drawing.Point(621, 378);
            this.btnSuperPower.Name = "btnSuperPower";
            this.btnSuperPower.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnSuperPower.Size = new System.Drawing.Size(296, 49);
            this.btnSuperPower.TabIndex = 41;
            this.btnSuperPower.Text = "Закинуть дрова в топку";
            this.btnSuperPower.UseVisualStyleBackColor = false;
            this.btnSuperPower.Click += new System.EventHandler(this.btnSuperPower_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(937, 648);
            this.Controls.Add(this.btnSuperPower);
            this.Controls.Add(this.btnAutoFilling);
            this.Controls.Add(this.grBChangeСlassicMetod);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.rTBResoults);
            this.Controls.Add(this.lblCountColumns);
            this.Controls.Add(this.lbCountRows);
            this.Controls.Add(this.nUDColumn);
            this.Controls.Add(this.nUDRow);
            this.Controls.Add(this.dGVMatrixOfEfficiency);
            this.Controls.Add(this.grBoxDopUslovia);
            this.Name = "Form1";
            this.Text = "Классические критерии принятия решений";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            ((System.ComponentModel.ISupportInitialize)(this.dGVMatrixOfEfficiency)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDRow)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDColumn)).EndInit();
            this.grBChangeСlassicMetod.ResumeLayout(false);
            this.grBChangeСlassicMetod.PerformLayout();
            this.grBoxDopUslovia.ResumeLayout(false);
            this.gBLeman.ResumeLayout(false);
            this.gBLeman.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGVDopOblastLeman)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dGVDopOblast)).EndInit();
            this.grBoxAlpha.ResumeLayout(false);
            this.grBoxAlpha.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dGVMatrixOfEfficiency;
        private System.Windows.Forms.NumericUpDown nUDRow;
        private System.Windows.Forms.NumericUpDown nUDColumn;
        private System.Windows.Forms.Label lbCountRows;
        private System.Windows.Forms.Label lblCountColumns;
        private System.Windows.Forms.RichTextBox rTBResoults;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.GroupBox grBChangeСlassicMetod;
        private System.Windows.Forms.RadioButton radBtnBaise;
        private System.Windows.Forms.RadioButton radBtnSavidj;
        private System.Windows.Forms.RadioButton radBtnValda;
        private System.Windows.Forms.RadioButton radBtnMaximax;
        private System.Windows.Forms.RadioButton radBtnLaplas;
        private System.Windows.Forms.RadioButton radBtnProizvedenei;
        private System.Windows.Forms.RadioButton radBtnGermeiera;
        private System.Windows.Forms.RadioButton radBtnHodjLeman;
        private System.Windows.Forms.RadioButton radBtnGurvitz;
        private System.Windows.Forms.GroupBox grBoxDopUslovia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAutoFilling;
        private System.Windows.Forms.Button btnSuperPower;
        private System.Windows.Forms.DataGridView dGVDopOblast;
        private System.Windows.Forms.GroupBox grBoxAlpha;
        private System.Windows.Forms.TextBox tBAlpha;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox gBLeman;
        private System.Windows.Forms.DataGridView dGVDopOblastLeman;
        private System.Windows.Forms.TextBox tBVi;
        private System.Windows.Forms.Label labelV;
    }
}

